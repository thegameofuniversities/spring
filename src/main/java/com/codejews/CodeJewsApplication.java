package com.codejews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeJewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeJewsApplication.class, args);
	}

}
