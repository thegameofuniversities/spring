package com.codejews.stats;

import com.codejews.booking.*;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.YearMonth;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/stats")
@AllArgsConstructor
public class StatsController {

    private final BookingService bookingService;

    @GetMapping("/booking-amount/{gameId}")
    public ResponseEntity getTotalBookingAmountForGame(@PathVariable long gameId) {
        BookingAmountDTO amount = bookingService.gameTotalBookingAmountTillNow(gameId);
        return ResponseEntity.ok(amount);
    }

    @GetMapping("/total-booking-amount")
    public ResponseEntity getTotalBookingAmounts() {
        List<Booking> bookings = bookingService.getAllBookings();
        List<StatsGameBookAmountDTO> stats = bookings.stream()
                .map(Booking::getGame)
                .distinct()
                .map(game -> {
                    long amount = bookings.stream().filter(booking -> booking.getGame().getId().equals(game.getId())).count();

                    StatsGameBookAmountDTO statsGameBookAmountDTO = new StatsGameBookAmountDTO();
                    statsGameBookAmountDTO.setGameId(game.getId());
                    statsGameBookAmountDTO.setGameName(game.getName());
                    statsGameBookAmountDTO.setAmount(amount);

                    return statsGameBookAmountDTO;
                })
                .collect(Collectors.toList());

        return ResponseEntity.ok(stats);
    }

    @GetMapping("/day-booking-amount/{year}/{month}")
    public ResponseEntity getDailyBookingAmount(@PathVariable Integer year, @PathVariable Integer month) {
        List<BookingDayAmountDTO> amount = bookingService.getBookingAmountAt(YearMonth.of(year, month));
        return ResponseEntity.ok(amount);
    }

    @GetMapping("/user-booking-amount/{userId}")
    public ResponseEntity getUserBookingAmount(@PathVariable Long userId) {
        UserBookingAmountDTO userBookingAmountDTO = bookingService.getTotalUserBookingAmount(userId);
        return ResponseEntity.ok(userBookingAmountDTO);
    }
}
