package com.codejews.stats;

import lombok.Data;

@Data
public class StatsGameBookAmountDTO {

    private Long gameId;
    private String gameName;
    private Long amount;

}
