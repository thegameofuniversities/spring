package com.codejews.user;

import com.codejews.booking.BookingListDTO;
import com.codejews.game.GameService;
import com.codejews.security.CurrentUserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

import static com.codejews.security.SecurityConstants.USER_ROLE;

@Controller
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final CurrentUserService currentUserService;
    private final GameService gameService;

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody User user) {
        if (user.getPassword() == null) {
            return ResponseEntity.badRequest().body("Password missing");
        }

        List<String> roles = Collections.singletonList(USER_ROLE);

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok().build();
    }

	@GetMapping("/bookings")
	public ResponseEntity<BookingListDTO> getBookings(@RequestParam Integer page, @RequestParam Integer size) {
		BookingListDTO result = new BookingListDTO();
		User currentUser = currentUserService.getCurrentUser();
		result.setGames(gameService.getMyGames(currentUser, PageRequest.of(page, size)));
		result.setCount(gameService.getMyGamesCount(currentUser));
		return ResponseEntity.ok(result);
	}
}
