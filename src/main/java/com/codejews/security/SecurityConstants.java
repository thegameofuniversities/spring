package com.codejews.security;

// Should be in properties file
public class SecurityConstants {
    static final long EXPIRATION_TIME = 1000 * 60 * 50; // 5 min
    static final String AUTHORITIES = "authorities";
    static final String ID = "id";
    static final String SECRET = "dkjgkerugu";
    static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER = "Authorization";

    public static final String USER_ROLE = "USER";
    public static final String ADMIN_ROLE = "ADMIN";

}
