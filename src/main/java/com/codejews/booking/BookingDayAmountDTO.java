package com.codejews.booking;

import lombok.Data;

@Data
public class BookingDayAmountDTO {

    private long time;
    private long amount;

}
