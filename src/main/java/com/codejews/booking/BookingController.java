package com.codejews.booking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping("/bookings")
public class BookingController {

    @Autowired
    BookingService bookingService;

    @GetMapping("/{id}/{year}/{month}")
    public ResponseEntity gets(@PathVariable Long id, @PathVariable Integer year, @PathVariable Integer month) {
        Optional<BookingResponseDTO> bookingResponseDTO = bookingService.occupiedDays(id, year, month);
        return bookingResponseDTO.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }
}
