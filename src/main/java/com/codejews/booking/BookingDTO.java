package com.codejews.booking;

import com.codejews.game.GameDTO;
import lombok.Data;

@Data
public class BookingDTO {
	Long start;
	Long end;
	GameDTO game;
}
