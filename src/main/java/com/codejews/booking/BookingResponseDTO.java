package com.codejews.booking;

import com.codejews.game.GameDTO;
import lombok.Data;

import java.util.Set;

@Data
public class BookingResponseDTO {
//    Set<Integer> reservedByMeAt;
    Set<Integer> reserved;
    Set<Integer> confirmed;
    Set<Integer> preemtable;
    Set<Integer> unavailable;
}
