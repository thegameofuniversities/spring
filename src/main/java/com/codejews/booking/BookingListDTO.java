package com.codejews.booking;

import lombok.Data;

import java.util.List;

@Data
public class BookingListDTO {

	private List<BookingDTO> games;
	private Long count;
}
