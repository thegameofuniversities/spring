package com.codejews.booking;

import com.codejews.game.Game;
import com.codejews.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
@Entity
@NoArgsConstructor
public class Booking {

    @Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	@JoinColumn(name = "game_id")
    private Game game;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

    private String info;

	@NotNull
	private Long startDate;

	@NotNull
	private Long endDate;

	@NotNull
	private Long approvesAt;

	public BookingDTO toBookingDTO() {
		BookingDTO result = new BookingDTO();
		result.setStart(startDate);
		result.setEnd(startDate);
		result.setGame(game.toGameDto(false));
		return result;
	}

	public boolean contains(LocalDateTime date) {
		Long timestamp = date.toEpochSecond(ZoneOffset.UTC);

		return startDate <= timestamp && timestamp <= endDate;
	}

	public boolean isApproved() {
		return Instant.now().getEpochSecond() > approvesAt;
	}
}
