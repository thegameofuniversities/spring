package com.codejews.booking;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BookingRepository extends PagingAndSortingRepository<Booking, Long> {
	Page<Booking> findAllByUserId(long userId, Pageable page);
	List<Booking> findAllByGameIdAndUserId(Long gameId, Long userId);
	List<Booking> findAllByGameIdAndStartDateLessThan(Long gameId, Long nowDate);
	List<Booking> findAll();

	@Query("SELECT b FROM Booking b WHERE b.game.id = ?3 and ((b.startDate >= ?1 and b.startDate <= ?2) or (b.endDate >= ?1 and b.endDate <= ?2) or (b.startDate <= ?1 and b.endDate >= ?2))")
	List<Booking> findBookingsInRangeForGame(long startDate, long endDate, long gameId);

	@Query("SELECT b FROM Booking b WHERE (b.startDate >= ?1 and b.startDate <= ?2) or (b.endDate >= ?1 and b.endDate <= ?2) or (b.startDate <= ?1 and b.endDate >= ?2)")
	List<Booking> findBookingsInRange(long startDate, long endDate);

	@Query("SELECT b FROM Booking b WHERE b.user.id = ?3 and ((b.startDate >= ?1 and b.startDate <= ?2) or (b.endDate >= ?1 and b.endDate <= ?2) or (b.startDate <= ?1 and b.endDate >= ?2))")
	List<Booking> findUserBookingsInRange(long startDate, long endDate, long userId);

	Long countBookingByUserId(long userId);
}
