package com.codejews.booking;

import lombok.Data;

@Data
public class UserBookingAmountDTO {

    private Long userId;
    private Long amount;

}
