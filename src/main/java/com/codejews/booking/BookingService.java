package com.codejews.booking;

import com.codejews.game.Game;
import com.codejews.game.GameRepository;
import com.codejews.security.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("Duplicates")
@Service
public class BookingService {

	@Autowired
	CurrentUserService currentUserService;
	@Autowired
	private GameRepository gameRepository;


	@Autowired
	private BookingRepository bookingRepository;

	public Optional<Booking> isPreemtable(Long start, Long end, Game game) {
		List<Booking> bookings = bookingRepository.findBookingsInRangeForGame(start, end, game.getId());
		long myGameCount = countGamesAt(start, end, currentUserService.getCurrentUser().getId());
		return bookings.stream()
				.filter(booking -> !booking.isApproved() && countGamesAt(start, end, booking.getUser().getId()) > myGameCount)
				.findAny();
	}

	private Set<Integer> getReservedByMeAt(Long gameId, YearMonth yearMonth) {
		List<Booking> bookings = bookingRepository.findAllByGameIdAndUserId(gameId, currentUserService.getCurrentUser().getId());

		return IntStream.range(1, yearMonth.lengthOfMonth() + 1).boxed()
				.map(day -> LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), day, 14, 0))
				.filter(date -> bookings.stream().anyMatch(booking -> booking.contains(date)))
				.map(LocalDateTime::getDayOfMonth)
				.collect(Collectors.toSet());
	}

	private Set<Integer> getPreemptible(Game game, YearMonth yearMonth) {
		LocalDateTime startDate = LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), 1, 0, 0);
		LocalDateTime endDate = LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), yearMonth.lengthOfMonth(), 0, 0);
		List<Booking> bookings = bookingRepository.findBookingsInRangeForGame(startDate.toEpochSecond(ZoneOffset.UTC), endDate.toEpochSecond(ZoneOffset.UTC), game.getId());

		return IntStream.range(1, yearMonth.lengthOfMonth() + 1).boxed()
				.map(day -> LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), day, 14, 0))
				.filter(date -> bookings.stream()
						.anyMatch(booking -> booking.contains(date) && isPreemtable(date.toEpochSecond(ZoneOffset.UTC), date.toEpochSecond(ZoneOffset.UTC) + 1, game).isPresent()))
				.map(LocalDateTime::getDayOfMonth)
				.collect(Collectors.toSet());
	}

	public long countGamesAt(Long start, Long end, Long userId) {
		return bookingRepository
				.findUserBookingsInRange(start, end, userId)
				.stream().map(Booking::getGame)
				.map(game -> Optional.ofNullable(game.getParent()).orElse(game))
				.distinct().count();

	}

	private Set<Integer> getUnavailable(Game game, YearMonth yearMonth) {
		LocalDateTime startDate = LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), 1, 0, 0);
		LocalDateTime endDate = LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), yearMonth.lengthOfMonth(), 0, 0);
		List<Booking> bookings = bookingRepository.findBookingsInRangeForGame(startDate.toEpochSecond(ZoneOffset.UTC), endDate.toEpochSecond(ZoneOffset.UTC), game.getId());

		return IntStream.range(1, yearMonth.lengthOfMonth() + 1).boxed()
				.map(day -> LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), day, 14, 0))
				.filter(date -> bookings.stream().filter(booking -> booking.contains(date) && !isPreemtable(date.toEpochSecond(ZoneOffset.UTC), date.toEpochSecond(ZoneOffset.UTC) + 1, game).isPresent()).count() >= game.getCount())
				.map(LocalDateTime::getDayOfMonth)
				.collect(Collectors.toSet());
	}

	private Set<Integer> getReserved(long gameId, YearMonth yearMonth) {
		List<Booking> bookings = bookingRepository.findAll().stream()
				.filter(booking -> booking.getGame().getId() == gameId)
				.filter(booking -> booking.getUser().getId().longValue() == currentUserService.getCurrentUser().getId().longValue())
				.collect(Collectors.toList());

		return IntStream.range(1, yearMonth.lengthOfMonth() + 1).boxed()
				.map(day -> LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), day, 14, 0))
				.filter(date -> bookings.stream()
						.filter(booking -> !booking.isApproved())
						.anyMatch(booking -> booking.contains(date)))
				.map(LocalDateTime::getDayOfMonth)
				.collect(Collectors.toSet());
	}

	private Set<Integer> getConfirmed(Long gameId, YearMonth yearMonth) {
		List<Booking> bookings = bookingRepository.findAllByGameIdAndUserId(gameId, currentUserService.getCurrentUser().getId());

		return IntStream.range(1, yearMonth.lengthOfMonth() + 1).boxed()
				.map(day -> LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), day, 14, 0))
				.filter(date -> bookings.stream().anyMatch(booking -> booking.contains(date) && booking.isApproved()))
				.map(LocalDateTime::getDayOfMonth)
				.collect(Collectors.toSet());
	}

	public List<BookingDayAmountDTO> getBookingAmountAt(YearMonth yearMonth) {
		LocalDateTime startDate = LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), 1, 0, 0);
		LocalDateTime endDate = LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), yearMonth.lengthOfMonth(), 0, 0);
		List<Booking> bookings = bookingRepository.findBookingsInRange(startDate.toEpochSecond(ZoneOffset.UTC), endDate.toEpochSecond(ZoneOffset.UTC));

		return IntStream.range(1, yearMonth.lengthOfMonth() + 1).boxed()
				.map(day -> LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonth().getValue(), day, 14, 0))
				.map(date -> {
					long amount = bookings.stream().filter(booking -> booking.contains(date)).count();
					long time = date.toEpochSecond(ZoneOffset.UTC);
					BookingDayAmountDTO bookingDayAmountDTO = new BookingDayAmountDTO();
					bookingDayAmountDTO.setAmount(amount);
					bookingDayAmountDTO.setTime(time);

					return bookingDayAmountDTO;
				})
				.collect(Collectors.toList());
	}

	public Optional<BookingResponseDTO> occupiedDays(Long id, Integer year, Integer month) {
		YearMonth yearMonth = YearMonth.of(year, month);

		return gameRepository.findById(id).map(game -> {
			BookingResponseDTO response = new BookingResponseDTO();
			response.setPreemtable(getPreemptible(game, yearMonth));
			response.setConfirmed(getConfirmed(id, yearMonth));
			response.setReserved(getReserved(id, yearMonth));
			response.setUnavailable(getUnavailable(game, yearMonth));
//			response.getUnavailable().removeAll(response.getPreemtable());
			return response;
		});
	}

	public BookingAmountDTO gameTotalBookingAmountTillNow(Long gameId) {
		Long now = Instant.now().getEpochSecond();
		int amount = bookingRepository.findAllByGameIdAndStartDateLessThan(gameId, now).size();

		BookingAmountDTO bookingAmountDTO = new BookingAmountDTO();
		bookingAmountDTO.setGameId(gameId);
		bookingAmountDTO.setAmount(amount);

		return bookingAmountDTO;
	}

	public UserBookingAmountDTO getTotalUserBookingAmount(Long userId) {
		long amount = bookingRepository.countBookingByUserId(userId);
		UserBookingAmountDTO bookingAmountDTO = new UserBookingAmountDTO();
		bookingAmountDTO.setUserId(userId);
		bookingAmountDTO.setAmount(amount);

		return bookingAmountDTO;
	}

	public List<Booking> getAllBookings() {
		return bookingRepository.findAll();
	}
}
