package com.codejews.booking;

import lombok.Data;

@Data
public class BookingAmountDTO {

    private Long gameId;

    private int amount;

}
