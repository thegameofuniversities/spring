package com.codejews.game;

import lombok.Data;

@Data
public class GameDTO {
	private long id;
	private String title;
	private String image;
	private String description;
	private boolean available;
	private Long ean;

}
