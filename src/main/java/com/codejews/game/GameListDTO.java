package com.codejews.game;

import lombok.Data;

import java.util.List;

@Data
public class GameListDTO {

	private List<GameDTO> games;
	private Long count;
}
