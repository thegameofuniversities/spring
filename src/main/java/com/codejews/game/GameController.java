package com.codejews.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/game")
public class GameController {

	@Autowired
	private GameService gameService;

	@GetMapping()
	public ResponseEntity<GameListDTO> get(@RequestParam Integer page, @RequestParam Integer size, @RequestParam(required = false) String query) {
		GameListDTO games = gameService.getGames(PageRequest.of(page, size), Optional.ofNullable(query));
		return ResponseEntity.ok(games);
	}

	@GetMapping("/names")
	public ResponseEntity<List<String>> getNames() {
		return ResponseEntity.ok(gameService.getGamesNames());
	}

	@GetMapping("/{id:[0-9]+}")
	public ResponseEntity<GameDescriptionDTO> get(@PathVariable Long id) {
		Optional<GameDescriptionDTO> games = gameService.getGame(id);
		return ResponseEntity.of(games);
	}

	@PostMapping("")
	public ResponseEntity post(@RequestBody GameDescriptionDTO game) {
		GameDescriptionDTO createdGame = gameService.createGame(game);
		return ResponseEntity.ok(createdGame);
	}

	@PostMapping("/book")
	public ResponseEntity reserve(@RequestBody ReservationDTO reservation) {
		if (reservation.getEnd() < reservation.getStart()) {
			return ResponseEntity.badRequest().build();
		}

//		if (reservation.getStart() < Instant.now(Clock.systemUTC()).getEpochSecond()) {
//			return ResponseEntity.badRequest().build();
//		}

		if (gameService.bookGames(reservation)) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
	}

	@PostMapping("/{id:[0-9]+}")
	public ResponseEntity update(@RequestBody GameDescriptionDTO game, @PathVariable Long id) {
		gameService.updateGame(game, id);
		return ResponseEntity.noContent().build(); //TODO: change to ok
	}

	@GetMapping("/ean/{ean}")
	public ResponseEntity getGame(@PathVariable Long ean) {
		Optional<GameDescriptionDTO> gameDescriptionDTO = gameService.getGameByCode(ean);
		return gameDescriptionDTO.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}
}
