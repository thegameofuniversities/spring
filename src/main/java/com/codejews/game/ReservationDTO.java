package com.codejews.game;

import lombok.Data;

import java.util.List;

@Data
public class ReservationDTO {
	private Long start;
	private Long end;
	private List<Long> games;
}
