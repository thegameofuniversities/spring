package com.codejews.game;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface GameRepository extends PagingAndSortingRepository<Game, Long> {
	List<Game> findAll();

	@Query("SELECT g FROM Game g WHERE lower(g.name) LIKE '%' || lower(?1) || '%'")
	Page<Game> findAllByName(Pageable page, String name);
	Optional<Game> findGameByEan(Long ean);
	List<Game> findAllByIdIn(Collection<Long> ids);

}
