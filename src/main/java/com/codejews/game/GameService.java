package com.codejews.game;

import com.codejews.booking.Booking;
import com.codejews.booking.BookingDTO;
import com.codejews.booking.BookingRepository;
import com.codejews.security.CurrentUserService;
import com.codejews.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings({"Duplicate", "Duplicates"})
@Service
public class GameService {

	@Autowired
	private GameRepository repo;

	@Autowired
	private BookingRepository bookingRepo;

	@Autowired
	private CurrentUserService cuService;

	public GameListDTO getGames(PageRequest page, Optional<String> query) {
		Page<Game> games = query.map(q -> repo.findAllByName(page, q)).orElseGet(() -> repo.findAll(page));
		GameListDTO result = new GameListDTO();
		result.setGames(games.stream().map(game -> game.toGameDto(isAvailableNow(game))).collect(Collectors.toList()));
		result.setCount(repo.count());
		return result;
	}

	private boolean isAvailableNow(Game game) {
		Long now = LocalDateTime.of(LocalDate.now(), LocalTime.of(14, 0)).toEpochSecond(ZoneOffset.UTC);
		return isAvailable(now, now + 1, game);
	}

	public List<BookingDTO> getMyGames(User currentUser, PageRequest page) {
		return bookingRepo
				.findAllByUserId(currentUser.getId(), page)
				.stream()
				.map(Booking::toBookingDTO)
				.collect(Collectors.toList());
	}

	public Optional<GameDescriptionDTO> getGame(Long id) {
		return repo.findById(id).map(game -> game.toGameDescriptionDto(isAvailableNow(game)));
	}

	public GameDescriptionDTO createGame(GameDescriptionDTO dto) {
		Game game = new Game();
		game.setDescription(dto.getDescription());
		game.setImage(dto.getImage());
		game.setName(dto.getTitle());
		game.setCount(dto.getCount());
		game.setBoardGameGeekUrl(dto.getUrl());
		game.setEan(dto.getEan());
		return repo.save(game).toGameDescriptionDto(true);
	}

	public void updateGame(GameDescriptionDTO dto, Long id) {
		repo.findById(id).ifPresent(game -> {
			game.setBoardGameGeekUrl(dto.getUrl());
			game.setDescription(dto.getDescription());
			game.setImage(dto.getImage());
			game.setName(dto.getTitle());
			game.setCount(dto.getCount());
			game.setEan(dto.getEan());
			repo.save(game);
		});
	}

	public boolean bookGames(ReservationDTO reservation) {
		List<Game> games = repo.findAllByIdIn(reservation.getGames());
		List<Game> occupiedGames = games.stream()
				.filter(game -> !isAvailable(reservation.getStart(), reservation.getEnd(), game))
				.collect(Collectors.toList());

		if (!occupiedGames.isEmpty()) {
			List<Booking> preemptibleBookings = occupiedGames.stream()
					.map(game -> isPreemtable(reservation.getStart(), reservation.getEnd(), game))
					.filter(Optional::isPresent)
					.map(Optional::get)
					.collect(Collectors.toList());

			if (preemptibleBookings.size() == occupiedGames.size()) {
				bookingRepo.deleteAll(preemptibleBookings);//TODO: push notification
			} else {
				return false;
			}
		}
		Long approvesAt = getApprovesAt(reservation.getStart());

		List<Booking> bookings = games.stream().map(game -> {
			Booking booking = new Booking();
			booking.setStartDate(reservation.getStart());
			booking.setEndDate(reservation.getEnd());
			booking.setGame(game);
			booking.setUser(cuService.getCurrentUser());
			booking.setApprovesAt(approvesAt);
			return booking;
		}).collect(Collectors.toList());

		bookingRepo.saveAll(bookings);

		return true;
	}

	private Long getApprovesAt(Long start) {
		LocalDate startDate = LocalDateTime.ofEpochSecond(start, 0, ZoneOffset.UTC).toLocalDate();
		LocalDate today = LocalDate.now(ZoneId.of("UTC"));
		if (startDate.toEpochDay() - today.toEpochDay() <= 1) {
			if (LocalTime.now(ZoneId.of("UTC")).getHour() >= 17) {
				return Instant.now(Clock.systemUTC()).getEpochSecond();
			} else {
				return Instant.now(Clock.systemUTC()).plus(Duration.ofMinutes(5)).getEpochSecond();
			}
		} else {
			return Instant.now(Clock.systemUTC()).plus(Duration.ofHours(2)).getEpochSecond();
		}
	}

	public boolean isAvailable(Long start, Long end, Game game) {
		List<Booking> bookings = bookingRepo.findBookingsInRangeForGame(start, end, game.getId());
		return bookings.size() < game.getCount();
	}

	public Optional<Booking> isPreemtable(Long start, Long end, Game game) {
		List<Booking> bookings = bookingRepo.findBookingsInRangeForGame(start, end, game.getId());
		long myGameCount = countGamesAt(start, end, cuService.getCurrentUser().getId());
		return bookings.stream()
				.filter(booking -> !booking.isApproved() && countGamesAt(start, end, booking.getUser().getId()) > myGameCount)
				.findAny();
	}


	public List<String> getGamesNames() {
		return repo.findAll().stream().map(Game::getName).collect(Collectors.toList());
	}

	public long countGamesAt(Long start, Long end, Long userId) {
		return bookingRepo
				.findUserBookingsInRange(start, end, userId)
				.stream().map(Booking::getGame)
				.map(game -> Optional.ofNullable(game.getParent()).orElse(game))
				.distinct().count();

	}

	public Optional<GameDescriptionDTO> getGameByCode(Long ean) {
		return repo.findGameByEan(ean).map(game -> game.toGameDescriptionDto(isAvailableNow(game)));
	}

	public Long getMyGamesCount(User currentUser) {
		return bookingRepo.countBookingByUserId(currentUser.getId());
	}
}
