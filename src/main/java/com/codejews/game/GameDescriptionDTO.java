package com.codejews.game;

import lombok.Data;

@Data
public class GameDescriptionDTO {
	private long id;
	private String title;
	private String image;
	private String description;
	private String url;
	private boolean available;
	private Long count;
	private Long ean;
}
