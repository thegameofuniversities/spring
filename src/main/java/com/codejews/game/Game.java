package com.codejews.game;

import com.codejews.booking.Booking;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@SuppressWarnings("Duplicates")
@Data
@Entity
@NoArgsConstructor
public class Game {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Game parent;

    @OneToMany(mappedBy = "parent")
    private List<Game> child;

    @OneToMany(mappedBy = "game")
    private List<Booking> bookings;

    @NotNull
    private String name;

	@Column(columnDefinition = "TEXT")
	private String image;

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description;

    @NotNull
    private Long count;

    private Long ean;

    private String boardGameGeekUrl;

    public GameDTO toGameDto(boolean isAvailable) {
        GameDTO result = new GameDTO();
        result.setId(id);
        result.setDescription(description);
        result.setImage(image);
        result.setTitle(name);
        result.setAvailable(isAvailable);
        result.setEan(ean);
        return result;
    }

    public GameDescriptionDTO toGameDescriptionDto(boolean isAvailable) {
        GameDescriptionDTO result = new GameDescriptionDTO();
        result.setId(id);
        result.setDescription(description);
        result.setImage(image);
        result.setTitle(name);
        result.setAvailable(isAvailable);
        result.setCount(count);
        result.setEan(ean);
        return result;
    }
}
